package siamux

import (
	"fmt"
	"io/ioutil"
	"path/filepath"
	"strings"
	"sync/atomic"
	"testing"
	"time"

	"gitlab.com/nitronick600/siacorn/persist"
	"gitlab.com/nitronick600/siamux/helpers"
	"gitlab.com/nitronick600/siamux/mux"
)

type muxTester struct {
	*SiaMux
}

func testDir(name string) string {
	return filepath.Join(helpers.TempDir(name))

}

// publicKey returns the server's public key used to sign the initial handshake.
func (mt *muxTester) publicKey() mux.ED25519PublicKey {
	return mt.staticPubKey
}

// newMuxTester creates a new SiaMux which is ready to be used.
func newMuxTester(testDir string) (*muxTester, error) {
	path := filepath.Join(testDir, persist.RandomSuffix())
	sm, err := New("127.0.0.1:0", persist.NewLogger(ioutil.Discard), path)
	if err != nil {
		return nil, err
	}
	return &muxTester{
		SiaMux: sm,
	}, nil
}

// TestNewSiaMux confirms that creating and closing a SiaMux works as expected.
func TestNewSiaMux(t *testing.T) {
	// Create SiaMux.
	sm, err := newMuxTester(testDir(t.Name()))
	if err != nil {
		t.Fatal(err)
	}
	// Check if the appSeed was set.
	if sm.staticAppSeed == 0 {
		t.Error("appSeed is 0")
	}
	// Close it again.
	if err := sm.Close(); err != nil {
		t.Fatal(err)
	}
}

// TestNewStream tests if registering a listener and connecting to it works as
// expected.
func TestNewStream(t *testing.T) {
	// Create 2 SiaMuxs and have one of them connect to the other one.
	client, err := newMuxTester(testDir(t.Name()))
	if err != nil {
		t.Fatal(err)
	}
	server, err := newMuxTester(testDir(t.Name()))
	if err != nil {
		t.Fatal(err)
	}

	// Prepare a handler to be registered by the server.
	var numHandlerCalls uint64
	handler := func(stream Stream) {
		atomic.AddUint64(&numHandlerCalls, 1)
		// Close the stream after handling it.
		if err := stream.Close(); err != nil {
			t.Fatal(err)
		}
	}
	// Try creating a new stream. This should fail with errUnknownSubscriber
	// since the server hasn't registered a handler yet.
	stream, err := client.NewStream("test", server.Address().String(), server.publicKey())
	if err != nil {
		t.Fatal(err)
	}
	_, err = stream.Read(make([]byte, 1))
	if err == nil || !strings.Contains(err.Error(), errUnknownSubscriber.Error()) {
		t.Fatal("error should be errUnknownSubscriber but was:", err)
	}
	if len(client.outgoingMuxs) != 1 {
		t.Fatalf("should have %v mux but was %v", 1, len(client.outgoingMuxs))
	}
	if len(client.muxs) != 1 {
		t.Fatalf("should have %v mux but was %v", 1, len(client.outgoingMuxs))
	}
	if len(client.muxSet) != 1 {
		t.Fatalf("should have %v mux but was %v", 1, len(client.outgoingMuxs))
	}
	// Register a listener.
	if err := server.NewListener("test", handler); err != nil {
		t.Fatal(err)
	}
	if len(server.handlers) != 1 {
		t.Fatalf("expected %v handler but got %v", 1, len(server.handlers))
	}
	// Try creating a new stream again. This time it should work.
	stream, err = client.NewStream("test", server.Address().String(), server.publicKey())
	if err != nil {
		t.Fatal(err)
	}
	// Check the fields of client and server. The client still has 1 mux since
	// it's reusing the already open one.
	if len(client.outgoingMuxs) != 1 {
		t.Fatalf("expected %v outgoing muxs but got %v", 1, len(client.outgoingMuxs))
	}
	if len(client.muxs) != 1 {
		t.Fatalf("expected %v total muxs but got %v", 1, len(client.muxs))
	}
	if len(client.muxSet) != 1 {
		t.Fatalf("expected %v total muxs but got %v", 1, len(client.muxSet))
	}
	if len(server.outgoingMuxs) != 0 {
		t.Fatalf("expected %v outgoing muxs but got %v", 0, len(server.outgoingMuxs))
	}
	if len(server.muxs) != 1 {
		t.Fatalf("expected %v total muxs but got %v", 1, len(server.muxs))
	}
	if len(server.muxSet) != 1 {
		t.Fatalf("expected %v total muxs but got %v", 1, len(server.muxSet))
	}
	// Check if the handler has been called exactly once so far. Need to do this
	// in a retry to avoid NDFs.
	err = helpers.Retry(100, 100*time.Millisecond, func() error {
		if numCalls := atomic.LoadUint64(&numHandlerCalls); numCalls != 1 {
			return fmt.Errorf("handler should've been called once but was %v", numCalls)
		}
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}
	// Delete the listener again. The stream should be closed.
	if err := server.CloseListener("test"); err != nil {
		t.Fatal(err)
	}
	if len(server.handlers) != 0 {
		t.Fatalf("expected %v handler but got %v", 0, len(server.handlers))
	}
	// Try creating a new stream one last time. This should fail with errUnknownSubscriber
	// since the server unregistered the handler.
	stream, err = client.NewStream("test", server.Address().String(), server.publicKey())
	if err != nil {
		t.Fatal(err)
	}
	_, err = stream.Read(make([]byte, 1))
	if err == nil || !strings.Contains(err.Error(), errUnknownSubscriber.Error()) {
		t.Fatal("error should be errUnknownSubscriber but was:", err)
	}
	// Close the stream.
	if err := stream.Close(); err != nil {
		t.Fatal(err)
	}
	// Check if the handler has been called exactly once again.
	if numCalls := atomic.LoadUint64(&numHandlerCalls); numCalls != 1 {
		t.Fatalf("handler should've been called once but was %v", numCalls)
	}
	// The server should still have 1 mux.
	if len(server.muxs) != 1 {
		t.Errorf("expected %v total muxs but got %v", 1, len(server.muxs))
	}
	if len(server.muxSet) != 1 {
		t.Errorf("expected %v total muxs but got %v", 1, len(server.muxSet))
	}
	// Simulate a timeout by closing the server's mux.
	for _, mux := range server.muxs {
		if err := mux.Close(); err != nil {
			t.Fatal(err)
		}
	}
	// The server should be back to having 0 muxs since closing the mux caused
	// it to remove it from the SiaMux.
	if len(server.outgoingMuxs) != 0 {
		t.Errorf("expected %v outgoing muxs but got %v", 0, len(server.outgoingMuxs))
	}
	if len(server.muxs) != 0 {
		t.Errorf("expected %v total muxs but got %v", 0, len(server.muxs))
	}
	if len(server.muxSet) != 0 {
		t.Errorf("expected %v total muxs but got %v", 0, len(server.muxSet))
	}
	// Since the server terminated the connection, the client should also be
	// cleaned up.
	err = helpers.Retry(100, 100*time.Millisecond, func() error {
		client.mu.Lock()
		defer client.mu.Unlock()
		if len(client.outgoingMuxs) != 0 {
			return fmt.Errorf("expected %v outgoing muxs but got %v", 0, len(client.outgoingMuxs))
		}
		if len(client.muxs) != 0 {
			return fmt.Errorf("expected %v total muxs but got %v", 0, len(client.muxs))
		}
		if len(client.muxSet) != 0 {
			return fmt.Errorf("expected %v total muxs but got %v", 0, len(client.muxSet))
		}
		return nil
	})
	if err != nil {
		t.Error(err)
	}
}
