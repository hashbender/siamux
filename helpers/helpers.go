package helpers

import (
	"os"
	"path/filepath"
	"time"
)

var (
	// SiaTestingDir is the directory that contains all of the files and
	// folders created during testing.
	SiaTestingDir = filepath.Join(os.TempDir(), "SiaTesting")
)

// Retry will call 'fn' 'tries' times, waiting 'durationBetweenAttempts'
// between each attempt, returning 'nil' the first time that 'fn' returns nil.
// If 'nil' is never returned, then the final error returned by 'fn' is
// returned.
func Retry(tries int, durationBetweenAttempts time.Duration, fn func() error) (err error) {
	for i := 1; i < tries; i++ {
		err = fn()
		if err == nil {
			return nil
		}
		time.Sleep(durationBetweenAttempts)
	}
	return fn()
}

// TempDir joins the provided directories and prefixes them with the Sia
// testing directory.
func TempDir(dirs ...string) string {
	path := filepath.Join(SiaTestingDir, filepath.Join(dirs...))
	os.RemoveAll(path) // remove old test data
	return path
}
